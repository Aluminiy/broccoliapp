﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BroccoliApp.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PinCodePage : ContentPage
	{
		public PinCodePage ()
		{
			InitializeComponent ();
            if (Device.Idiom == TargetIdiom.Desktop)
                ButtonsStackLayout.Orientation = StackOrientation.Horizontal;
		}
	}
}