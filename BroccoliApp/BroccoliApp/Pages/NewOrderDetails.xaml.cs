﻿using BroccoliApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BroccoliApp.Pages
{
	public partial class NewOrderDetails : ContentPage
	{
        public WalkInPageViewModel Vm { get; private set; }

        public NewOrderDetails ()
		{
			InitializeComponent ();

            Vm = App.Locator.WalkInPageViewModel;
            BindingContext = Vm;

            Vm.LoadCommand.Execute(null);
		}
	}
}