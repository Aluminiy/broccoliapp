﻿using BroccoliApp.Business.Interfaces;
using BroccoliApp.Business.Managers;
using BroccoliApp.ViewModels;
using GalaSoft.MvvmLight.Ioc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BroccoliApp.Pages
{
	public partial class LoginPage : ContentPage
	{
        public LoginPageViewModel Vm { get; private set; }

		public LoginPage ()
		{
			InitializeComponent ();

            Vm = App.Locator.LoginPageViewModel;
            BindingContext = Vm;
		}

        private async void LoginCommand(object sender, EventArgs e)
        {
            var result = await AuthManager.Login(Vm.Login, Vm.Password, CancellationToken.None);
            if (result.Error != null)
            {
                Vm.Error = result.Error.Message;
            }
            else
            {
                SettingsManager.AuthToken = result.Data;
                Application.Current.MainPage = new MainPage();
            }

        }
    }
}