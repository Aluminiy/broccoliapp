﻿using BroccoliApp.Business.Interfaces;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Input;

namespace BroccoliApp.ViewModels
{
    public class WalkInPageViewModel : ViewModelBase
    {
        public ICommand LoadCommand { get; private set; }

        private readonly IProductService _productService;
        private readonly IOrderService _orderService;

        //public ObservableCollection<Category> CategoryList;
        //public ObservableCollection<SubCategory> SubCategoryList;
        //public ObservableCollection<Product> ProductList;

        public WalkInPageViewModel(IProductService productService, IOrderService orderService)
        {
            _productService = productService;
            _orderService = orderService;

            LoadCommand = new RelayCommand(async () =>
            {
                var categories = await _productService.GetCategoryList(CancellationToken.None);

                if (categories.Error == null && categories.Data.Any())
                {
                    var subCategories = await _productService.GetSubCategoryList(categories.Data.First().Id, CancellationToken.None);

                    if (subCategories.Error == null && subCategories.Data.Any())
                    {
                        var products = await _productService.GetProductList(subCategories.Data.First().Id, CancellationToken.None);
                    }
                }
            });
        }
    }
}
