﻿using BroccoliApp.Business.Interfaces;
using BroccoliApp.Business.Services;
using GalaSoft.MvvmLight.Ioc;
using GalaSoft.MvvmLight.Views;
using System;
using System.Collections.Generic;
using System.Text;

namespace BroccoliApp.ViewModels
{
    public class ViewModelLocator
    {
        public ViewModelLocator()
        {
            var productService = new ProductService();
            SimpleIoc.Default.Register<IProductService>(() => productService);
            var orderService = new OrderService();
            SimpleIoc.Default.Register<IOrderService>(() => orderService);

            //var navigationService = CreateNavigationService();
            //SimpleIoc.Default.Register<INavigationService>(() => navigationService);

            SimpleIoc.Default.Register<LoginPageViewModel>(true);
            SimpleIoc.Default.Register<WalkInPageViewModel>(true);
        }

        private INavigationService CreateNavigationService()
        {
            var navigationService = new NavigationService();
            navigationService.Configure(Navigation.Pages.MainPage, typeof(Pages.MainPage));
            // navigationService.Configure("key1", typeof(OtherPage1));
            // navigationService.Configure("key2", typeof(OtherPage2));

            return navigationService;
        }

        public LoginPageViewModel LoginPageViewModel => SimpleIoc.Default.GetInstance<LoginPageViewModel>();
        public WalkInPageViewModel WalkInPageViewModel => SimpleIoc.Default.GetInstance<WalkInPageViewModel>();

    }
}
