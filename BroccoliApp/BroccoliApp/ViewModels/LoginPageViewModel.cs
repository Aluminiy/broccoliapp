﻿using BroccoliApp.Business.Interfaces;
using BroccoliApp.Business.Managers;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Ioc;
using GalaSoft.MvvmLight.Views;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Windows.Input;

namespace BroccoliApp.ViewModels
{
    public class LoginPageViewModel : ViewModelBase
    {
        public ICommand LoginCommand { get; private set; }
        private INavigationService _navigationService;

        private string _login;
        public string Login
        {
            get { return _login; }
            set { Set(ref _login, value); }
        }

        private string _password;
        public string Password
        {
            get { return _password; }
            set { Set(ref _password, value); }
        }

        private string _error;
        public string Error
        {
            get { return _error; }
            set { Set(ref _error, value); }
        }

        public LoginPageViewModel()
        {
            LoginCommand = new RelayCommand(async () => 
            {
                var productService = SimpleIoc.Default.GetInstance<IProductService>();

                var response = await productService.GetCategoryList(CancellationToken.None);

                var result = await AuthManager.Login(Login, Password, CancellationToken.None);
                if (result.Error != null)
                {
                    Error = result.Error.Message;
                }
                else
                {
                    SettingsManager.AuthToken = result.Data;
                }

            });
        }
    }
}
