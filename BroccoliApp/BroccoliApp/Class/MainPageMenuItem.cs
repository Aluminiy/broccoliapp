﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BroccoliApp.Class
{
    public class MainPageMenuItem
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public Type TargetType { get; set; }
    }
}
