﻿using BroccoliApp.Business.ApiClient;
using Newtonsoft.Json;
using Plugin.Settings;
using Plugin.Settings.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;

namespace BroccoliApp
{
    public static class SettingsManager
    {
        private static ISettings AppSettings
        {
            get
            {
                return CrossSettings.Current;
            }
        }

        #region Setting Constants

        private const string AuthTokenKey = "auth_token_key";
        private static readonly string AuthTokenDefault = string.Empty;

        #endregion

        public static AuthToken AuthToken
        {
            get
            {
                var serializedAuthToken = AppSettings.GetValueOrDefault(AuthTokenKey, AuthTokenDefault);
                try
                {
                    return JsonConvert.DeserializeObject<AuthToken>(serializedAuthToken);
                }
                catch (Exception)
                {
                    return null;
                }
            }
            set
            {
                try
                {
                    var serializedAuthToken = value != null ? JsonConvert.SerializeObject(value) : string.Empty;
                    AppSettings.AddOrUpdateValue(AuthTokenKey, serializedAuthToken);
                }
                catch (Exception) { }
            }
        }

    }
}
