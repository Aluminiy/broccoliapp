﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BroccoliApp.Business.Models
{
    public class SubCategory
    {
        public string Id { get; set; }
        public string CategoryId { get; set; }
        public string Name { get; set; }
        public string ImageName { get; set; }
        public string UpdatedAt { get; set; }
    }
}
