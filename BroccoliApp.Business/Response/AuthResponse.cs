﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace BroccoliApp.Business.Response
{
    [DataContract]
    public class AuthResponse
    {
        [DataMember(IsRequired = true, Name = "success")]
        public int Success { get; set; }

        [DataMember(IsRequired = false, Name = "token")]
        public string AccessToken { get; set; }

        [DataMember(IsRequired = false, Name = "error")]
        public string ErrorDesription { get; set; }

        [DataMember(Name = "data")]
        public AuthResponseData Data { get; set; }
    }

    [DataContract]
    public class AuthResponseData
    {
        [DataMember(Name = "_id")]
        public string Id { get; set; }

        [DataMember(Name = "email")]
        public string Email { get; set; }

        [DataMember(Name = "swipe_card_no")]
        public string SwipeCardNum { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "image")]
        public string Image { get; set; }

        [DataMember(Name = "user_unique_id")]
        public string UserUniqueId { get; set; }

        [DataMember(Name = "location_id")]
        public string LocationId { get; set; }

        [DataMember(Name = "role_id")]
        public string RoleId { get; set; }

        [DataMember(Name = "remember_token")]
        public string RememberToken { get; set; }

        [DataMember(Name = "is_deleted")]
        public bool IsDeleted { get; set; }
    }
}
