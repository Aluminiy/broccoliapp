﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace BroccoliApp.Business.Response
{
    [CollectionDataContract]
    public class ProductsResponse : CategoriesResponse<Product>
    {
        [DataMember(Name = "surcharge")]
        public IList<Surcharge> SurchargeList { get; set; }
    }

    [DataContract]
    public class Product
    {
        [DataMember(Name = "_id")]
        public string Id { get; set; }

        [DataMember(Name = "item_name")]
        public string Name { get; set; }

        [DataMember(Name = "item_barcode")]
        public string BarCode { get; set; }

        [DataMember(Name = "item_code")]
        public string Code { get; set; }

        [DataMember(Name = "item_price")]
        public double Price { get; set; }

        [DataMember(Name = "item_location")]
        public string Location { get; set; }

        [DataMember(Name = "item_description")]
        public string Description { get; set; }

        [DataMember(Name = "item_category")]
        public string Category { get; set; }

        [DataMember(Name = "item_subcategory")]
        public string SubCategory { get; set; }

        [DataMember(Name = "item_available_branches")]
        public string AvailableBranches { get; set; }

        [DataMember(Name = "item_status")]
        public int Status { get; set; }

        [DataMember(Name = "hide_in_screen")]
        public string HideInScreen { get; set; }

        [DataMember(Name = "show_on_web")]
        public string ShowOnWeb { get; set; }

        [DataMember(Name = "item_image")]
        public string Image { get; set; }

        [DataMember(Name = "updated_at")]
        public string UpdatedAt { get; set; }

        [DataMember(Name = "menu_location_tax")]
        public LocationTax LocationTax { get; set; }
    }

    [DataContract]
    public class LocationTax
    {
        [DataMember(Name = "_id")]
        public string Id { get; set; }

        [DataMember(Name = "location_id")]
        public string LocationId { get; set; }

        [DataMember(Name = "menu_item_id")]
        public string MenuItemID { get; set; }

        [DataMember(Name = "menu_location_tax")]
        public string MenuLocationTax { get; set; }

        [DataMember(Name = "tax_amount")]
        public double TaxAmount { get; set; }

        [DataMember(Name = "updated_at")]
        public string UpdatedAt { get; set; }
    }

    [DataContract]
    public class Surcharge
    {
        [DataMember(Name = "_id")]
        public string Id { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "type")]
        public string Type { get; set; }

        [DataMember(Name = "rate")]
        public double Rate { get; set; }

        [DataMember(Name = "enable_for")]
        public string EnableFor { get; set; }
    }
}
