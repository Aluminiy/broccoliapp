﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace BroccoliApp.Business.Response
{
    [DataContract]
    public class CategoriesResponse<T> : BaseResponse
    {
        [DataMember(Name = "status")]
        public int Status { get; set; }

        [DataMember(Name = "data")]
        public Data<T> Data { get; set; }
    }

    [DataContract]
    public class Data<T>
    {
        [DataMember(Name = "current_page")]
        public int CurrentPage { get; set; }

        [DataMember(Name = "first_page_url")]
        public string FirstPageUrl { get; set; }

        [DataMember(Name = "last_page_url")]
        public string LastPageUrl { get; set; }

        [DataMember(Name = "next_page_url")]
        public string NextPageUrl { get; set; }

        [DataMember(Name = "prev_page_url")]
        public string PrevPageUrl { get; set; }

        [DataMember(Name = "from")]
        public int From { get; set; }

        [DataMember(Name = "last_page")]
        public int LastPage { get; set; }

        [DataMember(Name = "per_page")]
        public int PerPage { get; set; }

        [DataMember(Name = "to")]
        public int To { get; set; }

        [DataMember(Name = "total")]
        public int Total { get; set; }

        [DataMember(Name = "data")]
        public IList<T> ObjectsList { get; set; }
    }

    [DataContract]
    public class Category
    {
        [DataMember(Name = "_id")]
        public string Id { get; set; }

        [DataMember(Name = "cat_name")]
        public string Name { get; set; }
    }

    [DataContract]
    public class SubCategory
    {
        [DataMember(Name = "_id")]
        public string Id { get; set; }

        [DataMember(Name = "menu_category_id")]
        public string CategoryId { get; set; }

        [DataMember(Name = "subcategory_name")]
        public string Name { get; set; }

        [DataMember(Name = "sub_category_image")]
        public string ImageName { get; set; }

        [DataMember(Name = "updated_at")]
        public string UpdatedAt { get; set; }
    }
}
