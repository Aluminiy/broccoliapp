﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BroccoliApp.Business.ApiClient
{
    public static class ApiServerUrl
    {
        public const string BaseUrl = "http://35.154.38.111/api";

        public const string AuthUrl = "auth/login";

        public const string GetCategoriesUrl = "auth/category/CatAndSubCatList";
        public const string GetSubCategoriesUrl = GetCategoriesUrl + "?category_id={0}";
        public const string GetProductsUrl = "auth/category/ProductList";
    }
}
