﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BroccoliApp.Business.ApiClient
{
    public class Response<T>
    {
        public T Data;
        public Error Error;

        public Response(T data, Error error)
        {
            Data = data;
            Error = error;
        }
    }
}
