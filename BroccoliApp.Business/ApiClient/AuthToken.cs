﻿using BroccoliApp.Business.Response;
using System;
using System.Collections.Generic;
using System.Text;

namespace BroccoliApp.Business.ApiClient
{
    public class AuthToken
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public string SwipeCardNum { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public string UserUniqueId { get; set; }
        public string LocationId { get; set; }
        public string RoleId { get; set; }
        public string RememberToken { get; set; }
        public bool IsDeleted { get; set; }
        public string AccessToken { get; set; }

        public void GetFromResponse(AuthResponse response)
        {
            Id = response.Data.Id;
            Email = response.Data.Email;
            SwipeCardNum = response.Data.SwipeCardNum;
            Name = response.Data.Name;
            Image = response.Data.Image;
            UserUniqueId = response.Data.UserUniqueId;
            LocationId = response.Data.LocationId;
            RoleId = response.Data.RoleId;
            RememberToken = response.Data.RememberToken;
            IsDeleted = response.Data.IsDeleted;
            AccessToken = response.AccessToken;
        }
    }

}
