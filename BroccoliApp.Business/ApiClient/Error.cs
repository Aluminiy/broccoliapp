﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BroccoliApp.Business.ApiClient
{
    public class Error
    {
        public enum ErrorType
        {
            Unknown,
            Business,
            DataAccess,
            Network,
            AuthNeeded
        }

        public string Message;
        public int Code;
        public ErrorType Type;

        public Error(ErrorType type, int code, string message)
        {
            Code = code;
            Type = type;
            Message = message;
        }
    }
}
