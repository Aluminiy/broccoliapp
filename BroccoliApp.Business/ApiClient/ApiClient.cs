﻿using BroccoliApp.Business.Response;
using RestSharp.Portable;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BroccoliApp.Business.ApiClient
{
    public class ApiClient
    {
        public const int ERR_INCORRECT_CREDENTIALS = 400;
        public const int ERR_INCORRECT_CREDENTIALS_401 = 401;
        public const int ERR_ACCESS_DENIED = 403;
        public const int ERR_BACKEND = 500;

        public ApiClient() { }

        public async Task<AuthResponse> Login(string login, string password, CancellationToken token)
        {
            var request = new RestRequest(ApiServerUrl.AuthUrl, Method.POST)
                .AddParameter("email", login)
                .AddParameter("password", password);

            using (var client = new WebClient.WebClient(ApiServerUrl.BaseUrl))
            {
                return await client.Execute<AuthResponse>(request, token);
            }
        }

        public async Task<CategoriesResponse<Category>> GetCategoryList(CancellationToken token)
        {
            var request = new RestRequest(ApiServerUrl.GetCategoriesUrl, Method.GET);
            request.AddHeader("Authorization", $"Bearer {SettingsManager.AuthToken.AccessToken}");

            using (var client = new WebClient.WebClient(ApiServerUrl.BaseUrl))
            {
                return await client.Execute<CategoriesResponse<Category>>(request, token);
            }
        }

        public async Task<CategoriesResponse<SubCategory>> GetSubCategoryList(string categoryId, CancellationToken token)
        {
            var request = new RestRequest(ApiServerUrl.GetCategoriesUrl, Method.GET);
            request.AddHeader("Authorization", $"Bearer {SettingsManager.AuthToken.AccessToken}");
            request.AddParameter("category_id", categoryId);

            using (var client = new WebClient.WebClient(ApiServerUrl.BaseUrl))
            {
                return await client.Execute<CategoriesResponse<SubCategory>>(request, token);
            }
        }

        public async Task<ProductsResponse> GetProductList(string subCategoryId, CancellationToken token)
        {
            var request = new RestRequest(ApiServerUrl.GetProductsUrl, Method.GET);
            request.AddHeader("Authorization", $"Bearer {SettingsManager.AuthToken.AccessToken}");
            request.AddParameter("subcategory_id", subCategoryId);

            using (var client = new WebClient.WebClient(ApiServerUrl.BaseUrl))
            {
                return await client.Execute<ProductsResponse>(request, token);
            }
        }

    }
}
