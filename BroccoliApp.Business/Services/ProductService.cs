﻿using BroccoliApp.Business.ApiClient;
using BroccoliApp.Business.Interfaces;
using BroccoliApp.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BroccoliApp.Business.Services
{
    public class ProductService : IProductService
    {
        private readonly HttpClient _client;

        public ProductService()
        {
            _client = new HttpClient();
        }

        public async Task<Response<List<Category>>> GetCategoryList(CancellationToken token)
        {
            List<Category> result = null;
            Error error = null;

            try
            {
                var client = new ApiClient.ApiClient();
                var response = await client.GetCategoryList(token);

                if (!string.IsNullOrWhiteSpace(response.Message))
                {
                    error = new Error(Error.ErrorType.Unknown, 0, response.Message);
                }
                else
                {
                    result = response.Data.ObjectsList.Select(c => new Category { Id = c.Id, Name = c.Name }).ToList();
                }
            }
            catch (Exception e)
            {
                error = new Error(Error.ErrorType.Unknown, 0, e.Message);
            }
            return new Response<List<Category>>(result, error);
        }

        public async Task<Response<List<Product>>> GetProductList(string subCategoryId, CancellationToken token)
        {
            List<Product> result = null;
            Error error = null;

            try
            {
                var client = new ApiClient.ApiClient();
                var response = await client.GetProductList(subCategoryId, token);

                if (!string.IsNullOrWhiteSpace(response.Message))
                {
                    error = new Error(Error.ErrorType.Unknown, 0, response.Message);
                }
                else
                {
                    //       result = response.Data.Categories.Select(c => new Category { Id = c.Id, Name = c.Name }).ToList();
                }
            }
            catch (Exception e)
            {
                error = new Error(Error.ErrorType.Unknown, 0, e.Message);
            }
            return new Response<List<Product>>(result, error);
        }

        public async Task<Response<List<SubCategory>>> GetSubCategoryList(string categoryId, CancellationToken token)
        {
            List<SubCategory> result = null;
            Error error = null;

            try
            {
                var client = new ApiClient.ApiClient();
                var response = await client.GetSubCategoryList(categoryId, token);

                if (!string.IsNullOrWhiteSpace(response.Message))
                {
                    error = new Error(Error.ErrorType.Unknown, 0, response.Message);
                }
                else
                {
                    result = response.Data.ObjectsList.Select(c => new SubCategory { Id = c.Id, Name = c.Name, CategoryId = c.CategoryId, ImageName = c.ImageName, UpdatedAt = c.UpdatedAt }).ToList();
                }
            }
            catch (Exception e)
            {
                error = new Error(Error.ErrorType.Unknown, 0, e.Message);
            }
            return new Response<List<SubCategory>>(result, error);
        }
    }
}
