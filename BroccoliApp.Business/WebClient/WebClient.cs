﻿using BroccoliApp.Business.WebClient.Exceptions;
using Newtonsoft.Json;
using RestSharp.Portable;
using RestSharp.Portable.HttpClient;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BroccoliApp.Business.WebClient
{
    class WebClient : IDisposable
    {
        static string USER_AGENT = string.Empty;
        protected RestClient mClient;

        public WebClient(string baseUrl)
        {
            mClient = new RestClient(baseUrl);
            mClient.IgnoreResponseStatusCode = true;
            mClient.UserAgent = USER_AGENT;
            mClient.Timeout = new TimeSpan(0, 0, 1000);
        }

        public void Dispose()
        {
            mClient.Dispose();
        }

        public async Task<T> Execute<T>(IRestRequest request, CancellationToken token)
        {
            bool isThereApiError = false;
            try
            {
                var response = await mClient.Execute(request, token);
                if (response.IsSuccess)
                {
                    return await Task.Factory.StartNew(() => JsonConvert.DeserializeObject<T>(response.Content));
                }
                else
                {
                    isThereApiError = true;
                    throw new WebClientException((int)response.StatusCode, response.StatusDescription);
                }
            }
            catch (Exception ex)
            {
                if (!isThereApiError)
                    Debug.WriteLine($"{ex.Message}: {ex.StackTrace}");
                throw;
            }
        }

        public async Task<IRestResponse> Execute(IRestRequest request, CancellationToken token)
        {
            bool isThereApiError = false;
            try
            {
                var response = await mClient.Execute(request, token);
                if (response.IsSuccess)
                {
                    return response;
                }
                else
                {
                    isThereApiError = true;
                    throw new WebClientException((int)response.StatusCode, response.StatusDescription);
                }
            }
            catch (Exception ex)
            {
                if (!isThereApiError)
                    Debug.WriteLine($"{ex.Message}: {ex.StackTrace}");
                throw;
            }
        }

    }
}
