﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BroccoliApp.Business.WebClient.Exceptions
{
    public class WebClientException : Exception
    {
        public int Code { get; set; }
        public string Description { get; set; }

        public WebClientException(int code, string description)
        {
            Code = code;
            Description = description;
        }
    }
}
