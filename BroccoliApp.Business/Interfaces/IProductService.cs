﻿using BroccoliApp.Business.ApiClient;
using BroccoliApp.Business.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BroccoliApp.Business.Interfaces
{
    public interface IProductService
    {
        Task<Response<List<Category>>> GetCategoryList(CancellationToken token);

        Task<Response<List<SubCategory>>> GetSubCategoryList(string categoryId, CancellationToken token);

        Task<Response<List<Product>>> GetProductList(string subCategoryId, CancellationToken token);
    }
}
