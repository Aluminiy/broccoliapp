﻿using BroccoliApp.Business.ApiClient;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BroccoliApp.Business.Managers
{
    public static class AuthManager
    {
        static AuthToken _currentToken;
        public static AuthToken CurrentToken { get { return _currentToken; } set { _currentToken = value; SettingsManager.AuthToken = value; } }

        public async static Task<Response<AuthToken>> Login(string login, string password, CancellationToken token)
        {
            AuthToken result = null;
            Error error = null;

            try
            {
                var client = new ApiClient.ApiClient();
                var response = await client.Login(login, password, token);

                result = new AuthToken();
                result.GetFromResponse(response);
                CurrentToken = result;

                if (response.Success == 0)
                {
                    error = new Error(Error.ErrorType.DataAccess, 0, response.ErrorDesription);
                }
            }
            catch (Exception e)
            {
                error = new Error(Error.ErrorType.Unknown, 0, e.Message);
            }

            return new Response<AuthToken>(result, error);
        }
    }
}
